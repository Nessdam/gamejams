using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StoneTrigger : MonoBehaviour
{
    public GameObject rock;
    public GameObject trigger;
    GameObject player;

    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.Find("Player");
    }

    // Update is called once per frame
    void Update()
    {
        if (player.transform.position.x - trigger.transform.position.x < 3 && player.transform.position.z - trigger.transform.position.z < 3)
        {
            rock.SetActive(true);
        }
    }
}
