using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StepSoundController : MonoBehaviour
{
    Transform player;
    AudioSource steps;
    Vector3 lastPosition;
    // Start is called before the first frame update
    void Start()
    {
        player = GetComponent<Transform>();
        steps = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        if (lastPosition != transform.position)
        {
            steps.mute = false;
        }
        else steps.mute = true;
        lastPosition = transform.position;
    }
}
