using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightController : MonoBehaviour
{
    public Light light;

    bool lightActive = false;
    // Start is called before the first frame update
    void Start()
    {
        //light = GetComponent<Light>();
        light.gameObject.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
       if (Input.GetKeyDown(KeyCode.F) && !lightActive)
        {
            light.gameObject.SetActive(true);
            lightActive = true;
        }
       else if (Input.GetKeyDown(KeyCode.F) && lightActive)
        {
            light.gameObject.SetActive(false);
            lightActive = false;
        }
    }
}
