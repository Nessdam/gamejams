using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    private CharacterController player;
    private Camera eyes;

    public int speed = 5, baseSpeed;

    public float sensitivity = 1000f;
    float rotX, rotY, moveLR, moveFB, lookUD = 0f;

    // Start is called before the first frame update
    void Start()
    {
        player = GetComponent<CharacterController>();
        eyes = GetComponentInChildren<Camera>();
    }

    // Update is called once per frame
    void Update()
    {
        Move();
    }

    //---------------------------------------------------------
    //                  Functions
    //---------------------------------------------------------
    private void Move()
    {
        moveFB = Input.GetAxis("Vertical") * speed;
        moveLR = Input.GetAxis("Horizontal") * speed;

        rotY = Input.GetAxis("Mouse Y") * sensitivity * Time.deltaTime;
        rotX = Input.GetAxis("Mouse X") * sensitivity * Time.deltaTime;

        lookUD -= rotY;
        lookUD = Mathf.Clamp(lookUD, -90f, 90f);

        Vector3 movement = new Vector3(moveLR, Physics.gravity.y, moveFB);
        transform.Rotate(Vector3.up * rotX);
        eyes.gameObject.transform.localRotation = Quaternion.Euler(lookUD, 0, 0);
        movement = transform.rotation * movement;
        player.Move(movement * Time.deltaTime);
    }
}
