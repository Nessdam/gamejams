# Game Jam

## Theme: Wisdom

## Restriction: weigth

We made a simple boulder puzzle game where the player is supposed to move two different types of boulders onto pressure plates in order to progress to the next level.

### Made by:

* Daniel Dahl
* Kristoffer Madsen

### 3D models from unity asset store by:

* Bit Glacier
* VK GameDev
* Broken Vector

### Music by:

* Rizwan Ashraf
