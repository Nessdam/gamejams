using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Music : MonoBehaviour
{
    private bool flag = false;

    private void Awake()
    {
        if (flag == false)
        {
            DontDestroyOnLoad(transform.gameObject);
            flag = true;
        }
    }
}
