using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    private CharacterController player;
    GameObject interactable;
    Vector3 lastMousePos;

    public int speed = 5, baseSpeed, jumpStrength = 100;
    public int interactRange = 2, legLength = 2;

    public float sensitivity = 4f, liftSpeed = 0.2f;
    float rotX, lift, moveLR, moveFB, moveUD;

    int boulderMask = 1 << 8, groundMask = 1 << 7;

    bool notJumped = true;
    private bool isHolding = false;

    // Start is called before the first frame update
    void Start()
    {
        player = GetComponent<CharacterController>();
        groundMask = ~groundMask;
        baseSpeed = speed;
    }

    // Update is called once per frame
    void Update()
    {
        Move();
        Sprint();
        Jump();
        BoulderInteraction();
    }

    private void FixedUpdate()
    {
        moveUD += Physics.gravity.y/2;
    }

    //---------------------------------------------------------
    //                  Functions
    //---------------------------------------------------------
    private void Move()
    {
        moveFB = Input.GetAxis("Vertical") * speed;
        moveLR = Input.GetAxis("Horizontal") * speed;

        Vector3 movement = new Vector3(moveLR, moveUD, moveFB);

        rotX = Input.GetAxis("Mouse X") * sensitivity;
        transform.Rotate(0, rotX, 0);

        movement = transform.rotation * movement;
        player.Move(movement * Time.deltaTime);
    }
    private void Jump()
    {
        if (Physics.Raycast(transform.position, Vector3.down, legLength, groundMask))
            notJumped = true;
        if (Input.GetKeyDown(KeyCode.Space) && notJumped)
        {
            moveUD = jumpStrength;
            notJumped = false;
        }
    }
    private void Sprint()
    {
        if (Input.GetKeyDown(KeyCode.LeftShift))
        {
            speed = 2 * speed;
        }
        if (Input.GetKeyUp(KeyCode.LeftShift))
        {
            speed = baseSpeed;
        }
    }

    private void BoulderInteraction()
    {
        RaycastHit hit;

        if (Input.GetKeyDown(KeyCode.E) && !isHolding)
        {
            if (Physics.Raycast(transform.position, transform.forward, out hit, interactRange, boulderMask))
            {
                interactable = hit.collider.gameObject;
                interactable.transform.parent = transform;
                interactable.GetComponent<Rigidbody>().isKinematic = true;
                isHolding = true;

            }
        }
        else if (Input.GetKeyDown(KeyCode.E) && isHolding)
        {
            interactable.transform.parent = null;
            interactable.GetComponent<Rigidbody>().isKinematic = false;
            isHolding = false;
        }
        if (isHolding && Input.mousePosition != lastMousePos)
        {
            lift = Input.GetAxis("Mouse Y") * liftSpeed;
            interactable.transform.position = interactable.transform.position + new Vector3(0, lift, 0);
        }
    }
    internal float GetSpeed()
    {
        return speed;
    }
}
