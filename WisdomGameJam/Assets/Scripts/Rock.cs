using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rock : MonoBehaviour
{
    private float lerp = 0, duration = 1;

    public bool isLight;
    public bool isHeavy;
    public bool onPreasurePlate = false;

    public GameObject[] pressurePlates;
    public GameObject closest;
    public GameObject plane;

    // Update is called once per frame
    void Update()
    {
        if (isHeavy && pressurePlates == null)
        {
            pressurePlates = GameObject.FindGameObjectsWithTag("HeavyPressurePlate");
        }
        else if (isLight && pressurePlates == null)
        {
            pressurePlates = GameObject.FindGameObjectsWithTag("LightPressurePlate");
        }

        if (pressurePlates != null)
        {
            closest = FindClosestPlate(pressurePlates);
            AnimateRock(closest);
        }
    }

    GameObject FindClosestPlate(GameObject[] plates)
    {
        float distance = Mathf.Infinity;
        Vector3 position = this.transform.position;

        foreach (GameObject plate in pressurePlates)
        {
            Vector3 diff = plate.transform.position - position;
            float curDistance = diff.sqrMagnitude;
            if (curDistance < distance)
            {
                closest = plate;
                distance = curDistance;
            }
        }

        return closest;
    }

    void AnimateRock(GameObject plate)
    {
        float distance = Vector3.Distance(this.transform.position, plate.transform.position);
        Vector3 endPos = new Vector3(plate.transform.position.x, this.transform.position.y, plate.transform.position.z);

        if (distance < 2.5f)
        {
            lerp += Time.deltaTime / duration;
            this.transform.position = Vector3.Lerp(this.transform.position, endPos, lerp);
            onPreasurePlate = true;
        }
    }
}
