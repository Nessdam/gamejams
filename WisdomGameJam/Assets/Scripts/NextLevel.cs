using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class NextLevel : MonoBehaviour
{
    
    GameObject[] smallRocks;
    GameObject[] largeRocks;
    private int goal;
    bool goalReached = false;

    // Start is called before the first frame update
    void Start()
    {
        smallRocks = GameObject.FindGameObjectsWithTag("SmallRock");
        largeRocks = GameObject.FindGameObjectsWithTag("BigRock");
        goal = smallRocks.Length + largeRocks.Length;
    }

    // Update is called once per frame
    void Update()
    {
        int count = 0;
        foreach(GameObject rock in smallRocks)
        {
            if(rock.GetComponent<Rock>().onPreasurePlate == true)
            {
                count++;
            }
        }
        foreach (GameObject rock in largeRocks)
        {
            if (rock.GetComponent<Rock>().onPreasurePlate == true)
            {
                count++;
            }
        }
        if (count == goal)
            goalReached = true;

        if (goalReached)
        {
            SceneManager.LoadScene("level2");
        }

    }
}
