using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractBase : MonoBehaviour
{
    public float radius = 3f;

    public string objectName;

    public string text;

    public PlayerController player;

    private void Start()
    {
        player = GetComponent<PlayerController>();
    }
    public virtual void OnInteract()
    {

    }
}
