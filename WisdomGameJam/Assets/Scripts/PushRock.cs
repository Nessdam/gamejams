using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PushRock : MonoBehaviour
{
    public float pushStrength = 6.0f;
    private PlayerController pc;

    private void Start()
    {
        pc = gameObject.GetComponent<PlayerController>();
    }

    void OnControllerColliderHit(ControllerColliderHit hit)
    {
        Rigidbody body = hit.collider.attachedRigidbody;

        if (body == null || body.isKinematic)
        {
            return;
        }

        if (hit.moveDirection.y < -0.3f)
        {
            return;
        }

        pushStrength = pc.GetSpeed();

        Vector3 direction = new Vector3(hit.moveDirection.x, 0, hit.moveDirection.z);

        body.velocity = (direction * pushStrength) / hit.rigidbody.mass;
    }
}
