using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MenuController : MonoBehaviour
{
    private Button restart;
    private Button exit;
    private Canvas canvas;
    
    // Start is called before the first frame update
    void Start()
    {
        canvas = GameObject.Find("Menu").GetComponent<Canvas>();
        canvas.enabled = false;
        restart = GameObject.Find("Restart").GetComponent<Button>();
        restart.onClick.AddListener(
            delegate
            {
                SceneManager.LoadScene(SceneManager.GetActiveScene().name);
            }
            );
        exit = GameObject.Find("Exit").GetComponent<Button>();
        exit.onClick.AddListener(
            delegate
            {
                Debug.Log("hit!");
                Application.Quit();
            }
            );
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape) && canvas.enabled == false)
        {
            canvas.enabled = true;

        }
        else if (Input.GetKeyDown(KeyCode.Escape) && canvas.enabled == true)
        {
            canvas.enabled = false;
        }
        
           
    }
}
